# ttf-eleven-windows

* Maintainer: Verdant Banana
* Contributor: Owen Trigueros
* Contributor: octocorvus
* Contributor: Vaporeon <vaporeon@vaporeon.io>
* Contributor: Zepman <the_zep_man@hotmail.com>
* Contributor: Michael Lass <bevan@bi-co.net>
* Contributor: Doug Newgard <scimmia at archlinux dot info>
* Contributor: reflexing <reflexing@reflexing.ru>
#
Instructions were copied from ttf-ms-win10-auto and ttf-ms-win11-auto then slightly modified.
## BUILD INSTRUCTIONS:
 Please note, that usage of Microsoft fonts outside running Windows
 system is prohibited by EULA (although in certain countries EULA is invalid).
 Please consult Microsoft license before using fonts.

 This PKGBUILD attempts to download fonts directly from Microsoft, by
 retrieving selective parts of the Windows 11 Enterprise 90-day evaluation
 edition. This only works if the user is allowed to mount filesystems through
 udisks2. This is determined by Polkit, which by default only allows users to
 do this when they are logged in locally (e.g. not through SSH).

 If it is possible to download fonts directly, around 200 MiB of data will be
 downloaded. Downloading the fonts this way can take 8-20 minutes, even on a
 fast connection. Be patient. Note that for this method, it is necessary to
 mount an HTTP source and an ISO file as a loop device using FUSE. If the
 build fails, it might be that these must be unmounted manually. This can be
 done with:

*  $ udisksctl unmount -b /dev/loopX
*  $ udisksctl loop-delete -b /dev/loopX
*  $ fusermount -uz src/mnt/http

 Replace /dev/loopX with the relevant loop device, which is reported during
 package build. If it isn't reported then loop device creation is not yet
 finished and it errored out due to timeout. It usually happens due to slow or
 inconsistent network. In such a case, you can do one of the following:
*  1. wait for the loop device to be created and run the above commands, or
*  2. just run the above 'fusermount' command (doing this might leave redundant
      loop devices which can be later removed with a simple system reboot);
      and then try building the package again.

 A file integrity check is performed after download. Due to the unconventional
 way that the data is downloaded, the verification is done in prepare().
#
 This package uses HTTPDirFS with permanent cache. Due to this the maximum
 download speed is around 15MiB/s. To disable cache, find and remove the
 '--cache' option from httpdirfs. Read more about HTTPDirFS permanent cache
 here: https://github.com/fangfufu/httpdirfs#permanent-cache-system

 https://aur.archlinux.org/packages/httpdirfs
#
 If fonts cannot be downloaded directly, the ISO fill will be fully
 downloaded. Due to that install.wim will be extracted from the ISO, it is
 assumed that twice its size (almost 8 GiB) is necessary as temporary disk
 space. A free disk space check is performed before the ISO is downloaded.
#
 Please ignore any 'ln' errors when building this package. This is expected
 behavior.
#
 If for some reason you want to download the full ISO file, please visit:

 https://www.microsoft.com/en-us/evalcenter/evaluate-windows-11-enterprise
#
 This package is based on ttf-ms-win11. Use that package if font files from
 a local source need to be used.
#
 ttf-ms-win10-auto and ttf-ms-win11-auto are considered to be upstream for this
 package, which is why its maintainers and contributors are added as
 contributors to this package. Without their effort this package would not
 exist, nor be updated.

